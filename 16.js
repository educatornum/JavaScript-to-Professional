// 16) Обьект гэж юу вэ, түүний өгөгдөлд хандах тухай
/*
    1. Primitive 
        Dotroo gants l utga aguulna

    2. Object
        {Key: value} 
            Name: Amartuvshin
            Major: Software Engineering
       Dotroo olon turliin utga aguuldag
*/

var h1 = {
  name: "Amartuvshin",
  major: "Software Enginneerng",
  Hobby: "Read a book",
};
var h2 = {
  name: "Manvi",
  major: "Art of Science",
  Hobby: "Watching TV",
};

console.log(h1.name + " =>" + h1.major + "=>" + h1.Hobby);
console.log(h2.name + " =>" + h2.major + "=>" + h2.Hobby);
